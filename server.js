import express from 'express';
import bodyParser from 'body-parser';
import bookRoutes from './Book/routes.js';
import fileUpload from 'express-fileupload';
import path from 'path';

const app = express();
const PORT = 3000;
const __dirname = path.resolve();
app.use(bodyParser.json());


app.use(fileUpload({
    parseNested: true,
    createParentPath: true
}));

app.use(express.static(__dirname + '/uploads'));
app.use(bookRoutes);

app.listen(PORT, function () {
    console.log('Express listening on port ' + PORT + '!');
});