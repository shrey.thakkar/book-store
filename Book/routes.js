import express from 'express';
const Router = express.Router();


import * as book from './controller.js';
import {middleware} from './middleware.js';

Router.get('/', function (req, res) {
    res.send('Book Store API ');
});

Router.get('/books', book.getAllBooks);
Router.post('/books/page', book.getAllBooks);
Router.get('/books/:uuid', book.getBookByuuid);
Router.post('/books',middleware.postValidatation, book.addBooks);
Router.put('/books/:uuid',middleware.putValidatation, book.updateBooks);
Router.delete('/books/:uuid', book.removeBooks);

export default Router;