import moment from "moment";
import _ from "underscore";
import { VALIDATION_SERVER_ERR } from "../utils/constants.js";
import { Response } from "../utils/helper.js";

const middleware = {
    postValidatation: function (req, res, next) {
        let body = _.pick(req.body, 'Name', 'Author', 'publisher', 'Edition', 'publishYear', 'Language');

        if (!body.Name || parseInt(body.Name) || body.Name.trim().length == 0) {
            return Response("Enter valid Book Name !", null, VALIDATION_SERVER_ERR, res);
        }
        if (!body.Author || parseInt(body.Author) || body.Author.trim().length == 0) {
            return Response("Enter Author's name properly !",null, VALIDATION_SERVER_ERR, res);
        }
        if (!body.publisher || parseInt(body.publisher) || body.publisher.trim().length == 0) {
            return Response("Enter publisher !!!",null, VALIDATION_SERVER_ERR, res);
        }
        if (!body.Edition || !parseInt(body.Edition)) {
            return Response("Book Edition is required (number)",null, VALIDATION_SERVER_ERR, res);
        }
        if (!body.publishYear || !parseInt(body.publishYear) || body.publishYear > moment().year()) {
            return Response("Enter valid publishYear !!!",null, VALIDATION_SERVER_ERR, res);
        }
        if (!body.Language || parseInt(body.Language) || body.Language.trim().length == 0) {
            return Response("Enter Book Language !!!",null, VALIDATION_SERVER_ERR, res);
        }
        if (req.files.hasOwnProperty('image')) {
            if (req.files.image.size > (5 * 1024 * 1024)) {
                return Response("File is not greater than 5MB", null, VALIDATION_SERVER_ERR, res);
            }else if (req.files.image.mimetype !== 'image/jpeg' && req.files.image.mimetype !== 'image/png' && req.files.image.mimetype !== 'image/jpg') {
                return Response("File must be in jpeg/png/jpg", null, VALIDATION_SERVER_ERR, res);
            }
        }
        next();
    },
    putValidatation: function (req, res, next) {
        let body = _.pick(req.body, 'Name', 'Author', 'publisher', 'Edition', 'publishYear', 'Language');

        if (body.hasOwnProperty('Name') && (parseInt(body.Name) || body.Name.trim().length == 0)) {
            return Response("Enter book name (string) and should not be null",null, VALIDATION_SERVER_ERR, res);
        }
        if (body.hasOwnProperty('Author') && (parseInt(body.Author) || body.Author.trim().length == 0)) {
            return Response("Enter book Author (string) and should not be null",null, VALIDATION_SERVER_ERR, res);
        }
        if (body.hasOwnProperty('publisher') && (parseInt(body.publisher) || body.publisher.trim().length == 0)) {
            return Response("Enter book publisher (string) and should not be null",null, VALIDATION_SERVER_ERR, res);
        }
        if (body.hasOwnProperty('Edition') && (body.Edition === 0 || !parseInt(body.Edition))){
            return Response("Enter proper Edition",null, VALIDATION_SERVER_ERR, res);
        }
        if (body.hasOwnProperty('publishYear') && (body.publishYear > moment().year() || body.publishYear === 0 || !parseInt(body.publishYear))) {
            return Response("Enter valid year",null, VALIDATION_SERVER_ERR, res);
        }
        if (body.hasOwnProperty('Language') && (parseInt(body.Language) || body.Language.trim().length == 0)) {
            return Response("Enter book Language (string) and should not be null",null, VALIDATION_SERVER_ERR, res);
        } 
        if (req.files.hasOwnProperty('image')) {
            if (req.files.image.size > (5 * 1024 * 1024)) {
                return Response("File is not greater than 5MB", null, VALIDATION_SERVER_ERR, res);
            }else if (req.files.image.mimetype !== 'image/jpeg' && req.files.image.mimetype !== 'image/png' && req.files.image.mimetype !== 'image/jpg') {
                return Response("File must be in jpeg/png/jpg", null, VALIDATION_SERVER_ERR, res);
            }
        }
        next();
    }
}

export {middleware};