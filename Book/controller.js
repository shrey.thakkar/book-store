import moment from 'moment';
import { INTERNAL_SERVER_ERR, NOT_FOUND_ERR, SUCCESS, VALIDATION_SERVER_ERR } from '../utils/constants.js';
import path from 'path';
import _ from 'underscore';
import { deleteimageToS3, pager, Response, uploadimageToS3 } from '../utils/helper.js';
const books = [];

import { uuid } from 'uuidv4';
import { write_file } from '../utils/helper.js';
import { bookMsg, internalServerError } from '../utils/messages.js';



const getAllBooks = function (req, res) {
    try {
        let page = req.body.page ? req.body.page : 1;
        let record_per_page = req.body.record_per_page ? req.body.record_per_page : 10;

        if (typeof page !== 'number' || typeof record_per_page !== 'number') {
            return Response("enter page number properly", null, VALIDATION_SERVER_ERR, res);
        }

        let startPage = (page - 1) * record_per_page;
        let endPage = page * record_per_page;


        return pager(books.slice(startPage, endPage), page, record_per_page, books.length, res);
    }
    catch (e) {
        return Response(internalServerError, null, INTERNAL_SERVER_ERR, res);
    }
};


const getBookByuuid = function (req, res) {
    try {
        let bookuuID = req.params.uuid;
        let matchedBook;

        books.forEach(function (book) {
            if (bookuuID === book.uuid && book.deletedAt === null) {
                matchedBook = book;
            }
        });

        if (matchedBook) {
            return Response(bookMsg.List, matchedBook, SUCCESS, res);
        } else {
            return Response(bookMsg.not_found, null, NOT_FOUND_ERR, res);
        }
    }
    catch (e) {
        return Response(internalServerError, null, INTERNAL_SERVER_ERR, res);
    }
};

const addBooks = function (req, res) {
    try {
        let body = _.pick(req.body, 'Name', 'Author', 'publisher', 'Edition', 'publishYear', 'Language');
        let image = req.files.image ? req.files.image : null;
        body.Edition = body.Edition.toString();
        body.publishYear = body.publishYear.toString();
        body.uuid = uuid();
        body.createdAt = moment.utc().local().format('YYYY-MM-DD HH:mm:ss');
        body.updatedAt = moment.utc().local().format('YYYY-MM-DD HH:mm:ss');
        body.deletedAt = null;


        if (image === null) {
            body.image = null;
        } else {
            let fileExtension = path.extname(image.name);
            let imageName = 'img-' + Date.now() + fileExtension;
            // let imagePath = './uploads/' + imageName;
            // image.mv(imagePath, function (err) {
            //     if (err) {
            //         console.log(err);
            //     }
            // });

            let bufferFile = Buffer.from(image.data, "binary");

            let putObjectPromise = uploadimageToS3(imageName, bufferFile);
            putObjectPromise.then((data) => {
                body.image = 'http://smartsense-nodejs-poc.s3.ap-south-1.amazonaws.com/' + imageName;
                books.push(body);

                write_file(books);

                return Response(bookMsg.created, body, SUCCESS, res);
            }).catch((e) => {
                return Response(e, null, VALIDATION_SERVER_ERR, res);
            });

            // body.image = req.protocol + '://' + req.get('host') + '/' + image.name;
        }

    }
    catch (e) {
        return Response(internalServerError, null, INTERNAL_SERVER_ERR, res);
    }

};


const updateBooks = function (req, res) {

    try {
        let bookuuID = req.params.uuid;
        let matchedBook;

        books.forEach(function (book) {
            if (bookuuID === book.uuid && book.deletedAt === null) {
                matchedBook = book;
            }
        });
        if (!matchedBook) {
            return Response(bookMsg.not_found, null, NOT_FOUND_ERR, res);
        }
        let body = _.pick(req.body, 'Name', 'Author', 'publisher', 'Edition', 'publishYear', 'Language');
        let image = req.files.image ? req.files.image : matchedBook.image;
        let attributes = {};

        for (let keys in body) {
            attributes[keys] = body[keys];
        }

        if (!req.files.image) {
            attributes.image = matchedBook.image;
        }
        else {
            let fileExtension = path.extname(image.name);
            let imageName = 'img-' + Date.now() + fileExtension;
            // let imagePath = './uploads/' + image.name;
            // image.mv(imagePath, function (err) {
            //     if (err) {
            //         console.log(err);
            //     }
            // });
            let url = matchedBook.image;
            // let oldFile = './uploads/' + url.substring(url.lastIndexOf('/') + 1);

            // //moveFile(oldFile, './Deleted/' + url.substring(url.lastIndexOf('/') + 1));
            // fs.unlink(oldFile, function (err) {
            //     if (err) throw err
            //     console.log('Deleted ');
            // })
            // attributes.image = req.protocol + '://' + req.get('host') + '/' + image.name;
            let keyname = url.substring(url.lastIndexOf('/') + 1);


            let deleteObjectPromise = deleteimageToS3(keyname);
            deleteObjectPromise.then().catch((e) => {
                return Response(e, null, VALIDATION_SERVER_ERR, res);
            });


            let bufferFile = Buffer.from(image.data, "binary");

            let putObjectPromise = uploadimageToS3(imageName, bufferFile);
            putObjectPromise.then((data) => {

                attributes.image = 'http://smartsense-nodejs-poc.s3.ap-south-1.amazonaws.com/' + imageName;

                _.extend(matchedBook, attributes);
                matchedBook.updatedAt = moment.utc().local().format('YYYY-MM-DD HH:mm:ss');

                write_file(books);

                return Response(bookMsg.updated, matchedBook, SUCCESS, res);
            }).catch((e) => {
                return Response(e, null, VALIDATION_SERVER_ERR, res);
            });
        }
    }
    catch (e) {
        return Response(internalServerError, null, INTERNAL_SERVER_ERR, res);
    }
};


const removeBooks = function (req, res) {
    try {
        let bookUuid = req.params.uuid;
        let matchedBook = _.findWhere(books, { uuid: bookUuid });

        if (!matchedBook || matchedBook.deletedAt !== null) {
            return Response(bookMsg.not_found, null, NOT_FOUND_ERR, res);
        }
        if (matchedBook.image !== null) {
            let url = matchedBook.image;
            // let imagePath = './uploads/' + url.substring(url.lastIndexOf('/') + 1);
            // moveFile(imagePath, './Deleted/' + url.substring(url.lastIndexOf('/') + 1));

            let keyname = url.substring(url.lastIndexOf('/') + 1);


            let deleteObjectPromise = deleteimageToS3(keyname);
            deleteObjectPromise.then((data) => {
                matchedBook.deletedAt = moment.utc().local().format('YYYY-MM-DD HH:mm:ss')

                write_file(books);

                matchedBook.image = null;
                return Response(bookMsg.deleted, matchedBook, SUCCESS, res);
            }).catch((e) => {
                return Response(e, null, VALIDATION_SERVER_ERR, res);
            });



        }
    }
    catch (e) {
        return Response(internalServerError, null, INTERNAL_SERVER_ERR, res);
    }
};

export { addBooks, removeBooks, updateBooks, getAllBooks, getBookByuuid };
