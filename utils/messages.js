export const bookMsg = {
    "List": 'Book Data',
    "created": 'Book added successfully',
    "updated": 'Book updated successfully',
    "deleted": 'Book removed successfully',
    "not_found": "This ID is not in scope !!"
};

export const internalServerError = 'Server error !!!';