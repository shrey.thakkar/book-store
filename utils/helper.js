import fs from 'fs';
// import express from "express";
import res from "express/lib/response.js";
import S3 from 'aws-sdk/clients/s3.js';
import 'dotenv/config';

export function write_file(data) {
    fs.writeFile("./data.json", JSON.stringify(data), function (err) {
        if (err) throw err;
    });
}

export function Response(message, Data , status, res){
    let res_structure = {
        "status": status,
        "message": message
    };
    if (res_structure.status == 200) {
        res_structure.payload = Data
    }
    return res.send(res_structure);
}

export function pager(data, page, record_per_page, overallRecords, res) {
    return res.send({
        "status": 200,
        "message": "Book Data: ",
        "payload": data,
        "pager": {
            "recordsPerPage": record_per_page,
            "totalRecords": overallRecords,
            "page": page
        }
    });
}

const bucketName = process.env.AWS_BUCKET_NAME
const region = process.env.AWS_BUCKET_REGION
const accessKeyId = process.env.AWS_ACCESS_KEY
const secretAccessKey = process.env.AWS_SECRET_KEY

const s3 = new S3({
    region,
    accessKeyId,
    secretAccessKey
})

export function uploadimageToS3(file, data) {
    const uploadParams = {
        Bucket: bucketName,
        Key: file,
        Body: data
    }

    return s3.putObject(uploadParams).promise()
}

export function deleteimageToS3(file){
    const deleteParams = {
        Bucket: bucketName,
        Key: file
    };

    return s3.deleteObject(deleteParams).promise();
}
// export let error = (res) => {
    //     return res.send({
        //         "status": 500,
        //         "message": "Internal server error"
        //     });
        // }
        
        // export let validation = (message, res) => {
            //     return res.send({
                //             "status": 422,
                //             "message": message
                //         });
                // }
                