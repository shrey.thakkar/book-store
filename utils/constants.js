export const SUCCESS = 200

export const INTERNAL_SERVER_ERR = 500;

export const VALIDATION_SERVER_ERR = 422;

export const NOT_FOUND_ERR = 404;